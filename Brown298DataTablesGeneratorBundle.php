<?php
namespace Brown298\DataTablesGeneratorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class Brown298DataTablesGeneratorBundle
 *
 * @package Brown298\DataTablesGeneratorBundle
 * @author  John Brown <brown.john@gmail.com>
 */
class Brown298DataTablesGeneratorBundle extends Bundle
{
    /**
     * getParent
     *
     * @return string
     */
    public function getParent()
    {
        return 'SensioGeneratorBundle';
    }
} 