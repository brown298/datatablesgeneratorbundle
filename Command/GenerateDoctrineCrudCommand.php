<?php
namespace Brown298\DataTablesGeneratorBundle\Command;

use Brown298\DataTablesGeneratorBundle\Generator\DoctrineCrudGenerator;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Sensio\Bundle\GeneratorBundle\Command\GenerateDoctrineCrudCommand as BaseGenerateDoctrineCrudCommand;

/**
 * Class GenerateDoctrineCrudCommand
 *
 * @package Sensio\Bundle\GeneratorBundle\Command
 * @author  John Brown <brown.john@gmail.com>
 */
class GenerateDoctrineCrudCommand extends BaseGenerateDoctrineCrudCommand
{

    /**
     * getSkeletonDirs
     *
     * @param BundleInterface $bundle
     *
     * @return array
     */
    protected function getSkeletonDirs(BundleInterface $bundle = null)
    {
        $skeletonDirs  = array();
        $baseSkeletons = parent::getSkeletonDirs($bundle);

        if (isset($bundle) && is_dir($dir = $bundle->getPath().'/Resources/SensioGeneratorBundle/skeleton')) {
            $skeletonDirs[] = $dir;
        }

        if (is_dir($dir = $this->getContainer()->get('kernel')->getRootdir().'/Resources/SensioGeneratorBundle/skeleton')) {
            $skeletonDirs[] = $dir;
        }

        $skeletonDirs[] = __DIR__.'/../Resources/skeleton';
        $skeletonDirs[] = __DIR__.'/../Resources';
        $skeletonDirs = array_merge($skeletonDirs, $baseSkeletons);
        return $skeletonDirs;
    }

    /**
     * createGenerator
     *
     * @param null $bundle
     *
     * @return DoctrineCrudGenerator|\Sensio\Bundle\GeneratorBundle\Generator\DoctrineCrudGenerator
     */
    protected function createGenerator($bundle = null)
    {
        return new DoctrineCrudGenerator($this->getContainer()->get('filesystem'));
    }

}

